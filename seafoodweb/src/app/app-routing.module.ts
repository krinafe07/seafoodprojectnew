import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddDishComponent } from './Components/Admin/Dishes/add-dish/add-dish.component';
import { UpdateDishComponent } from './Components/Admin/Dishes/add-dish/update-dish/update-dish.component';
import { DeleteDishComponent } from './Components/Admin/Dishes/delete-dish/delete-dish.component';
import { AddUserComponent } from './Components/Admin/User/add-user/add-user.component';
import { DeleteUserComponent } from './Components/Admin/User/delete-user/delete-user.component';
import { UpdateUserComponent } from './Components/Admin/User/update-user/update-user.component';


const routes: Routes = [
  // {path:'home', component:}
  {path:'addDish',component:AddDishComponent},
  {path:'updateDish',component:UpdateDishComponent},
  {path:'deleteDish',component:DeleteDishComponent},
  {path:'addUser',component:AddUserComponent},
  {path:'deleteUser',component:DeleteUserComponent},
  {path:'updateUser',component:UpdateUserComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
